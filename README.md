# HoloChat  

![HoloChat Preview](https://holochat.app/assets/images/holochat.gif)

Realtime chat app designed for the looking glass portrait but works in desktop as well with orbit controls.

Uses [MediaPipe](https://google.github.io/mediapipe/) to create a realtime depth map from a webcam image. This map is then used to displace a plane in [ThreeJS](http://threejs.org). Orbit controls are available for rotating the final render and quilting for displaying inside a [Looking Glass](https://lookingglassfactory.com/).
