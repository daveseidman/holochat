import { addEl, createEl } from 'lmnt';

export default class About {
  constructor() {
    this.el = createEl('div', { className: 'about hidden' });
    this.back = createEl('div', { className: 'about-back' }, {}, { click: () => { this.el.classList.add('hidden'); } });
    this.icon = createEl('div', { className: 'about-icon', innerText: 'i' }, {}, { click: () => { this.el.classList.remove('hidden'); } });
    this.modal = createEl('div', { className: 'about-modal',
      innerHTML:
    'HoloChat is a WebGL Experiment by <a target="_blank" href="http://daveseidman.com">Dave Seidman</a>. '
    + 'It requires a decent graphics card to run smoothly. '
    + 'The user\'s webcam is extruded in z-space using a depth map created by combining data returned from Face Detection and Selfie Segmentation solutions by <a target="_blank" href="https://google.github.io/mediapipe/solutions/solutions.html">MediaPipe</a>.<br> '
    + 'The resulting 3D plane can be orbited or viewed as a Hologram inside of a <a target="_blank" href="https://lookingglassfactory.com/">Looking Glass</a>.<br><br>'
  + 'Souce: <a target="_blank" href="https://gitlab.com/daveseidman/holoport">https://gitlab.com/daveseidman/holochat</a>' });

    addEl(this.el, this.back, this.icon, this.modal);
  }
}
